// Libraries
import angular from 'angular';
import 'angular-ui-router';
import _ from 'lodash'; // eslint-disable-line
import moment from 'moment'; // eslint-disable-line
import 'angular-moment/angular-moment';
import geolib from 'geolib';

// Services
import mapService from './app/services/mapService';

// Components
import appHeaderComponent from './app/components/app-header/app-header';
import mapComponent from './app/components/map/map';
import cardsListComponent from './app/components/cards-list/cards-list';

// Pages
import landingPage from './app/pages/landing-page/landing-page';
import nearMePageComponent from './app/pages/near-me/near-me';

// Routes
import routesConfig from './routes';
import onRun from './onRun';

// Styles
import './assets/styles/index.scss';

export const app = 'app';

window.geolib = geolib;  // eslint-disable-line

angular
  .module(app, ['ui.router', 'angularMoment'])
  .config(routesConfig)
  .run(onRun)
  .factory('mapService', mapService)
  .component('app', landingPage)
  .component('nearMePage', nearMePageComponent)
  .component('appHeader', appHeaderComponent)
  .component('map', mapComponent)
  .component('cardsList', cardsListComponent);
