export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('home', {
      url: '/',
      component: 'app'
    });

  $stateProvider
    .state('whatToDo', {
      url: '/what-to-do',
      component: 'whatToDoPage'
    });

  $stateProvider
    .state('nearMe', {
      url: '/near-me',
      component: 'nearMePage'
    });
}
