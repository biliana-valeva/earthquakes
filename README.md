# Earthquakes

## Setup
> You will need node, npm and gulp

Run `npm install` to install all project dependencies

## Local server
To run local server run `gulp serve` (need to install gulp globally) or `npm run serve` and view the site at http://localhost:3000

## Styles
SCSS is used for styling in this project. You can find all styles in `app/styles/`.
All partials are categorized into the following groups:
* core: All base styles go here, such as vaiables, mixins, placeholders, resets, typography etc.
* ui-element: This category includes all ui elements such as headlines, checkboxes, forms fields etc.
* components: This category includes bigger building blocks, which can consist of ui elements. Ideally components styles partials names will correspond to the components names in `app/components`.
In order to add more styles, create partials in the corresponding category. If category doesn't exist, create a folder for it and add an index.scss file to include all partials in it.
All index files from all categories are then included in `assets/styles/index.scss`.

## Functionality
This app uses Angular 1.5. All factories and services should go in `app/services`. Components is the place to include building blocks that can be included on pages.

## Build
Run `gulp build` to build the project

