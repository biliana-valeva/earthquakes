const nearMePageTemplate = require('./near-me.html');
const nearMePageController = function ($state, $http, $window, $timeout, moment) {
  const getEarthquakeDataUrl = 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson';
  this.linkDisabled = $state.includes('nearMe');
  this.distance = 500;
  this.loading = true;
  this.error = false;
  this.earthquakes = [];
  this.earthquakesNearBy = [];

  this.formatTimeToUtc = time => {
    return moment.utc(time).format('DD.MM.YYYY HH:mm:ss');
  };

  $http({
    method: 'GET',
    url: getEarthquakeDataUrl
  }).then(response => {
    this.earthquakes = response.data.features;

    if (!navigator.geolocation) {
      $timeout(() => {
        this.loading = false;
      });

      return false;
    }

    navigator.geolocation.getCurrentPosition(position => {
      // Since luckily there are no earthquakes near by
      // toggle comments in following lines to see list of nearby earthquakes
      const userPositionLatitude = position.coords.latitude;
      const userPositionLongitude = position.coords.longitude;
      // Sets location to San Diego, CA
      // const userPositionLatitude = 32.715738;
      // const userPositionLongitude = -117.161084;

      // Mapbox uses lng lat order
      angular.forEach(this.earthquakes, item => {
        let distanceFromUser = $window.geolib.getDistance(
          {latitude: userPositionLatitude, longitude: userPositionLongitude},
          {latitude: item.geometry.coordinates[1], longitude: item.geometry.coordinates[0]
        });
        distanceFromUser /= 1000;

        if (distanceFromUser < this.distance) {
          $timeout(() => {
            this.earthquakesNearBy.push(item);
          });
        }
      });

      $timeout(() => {
        this.loading = false;
      });
    }, () => {
      $timeout(() => {
        this.locationUnkown = true;
        this.loading = false;
      });
    });
  }, () => {
    this.loading = false;
    this.error = true;
  });
};

const nearMePageComponent = {
  template: nearMePageTemplate,
  controller: nearMePageController
};

export default nearMePageComponent;
