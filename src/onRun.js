export default onRun;

/** @ngInject */
function onRun($window) {
  // Set mapbox token
  $window.mapboxgl.accessToken = 'pk.eyJ1IjoiYmlsaWFuYSIsImEiOiJjaXRxeWM0NDkwMDA0MnNtcnNkaTNjbXJxIn0.9hPFe_b23PXKI0rM3I-6qw';
}
