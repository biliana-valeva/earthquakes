const cardsListTemplate = require('./cards-list.html');

const cardsListComponent = {
  template: cardsListTemplate,
  bindings: {
    cardsdata: '<'
  }
};

export default cardsListComponent;
