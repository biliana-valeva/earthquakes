const mapTemplate = require('./map.html');

const mapController = function ($http, $window, moment, mapService) {
  'ngInject';

  // GeoJson Map Data Url
  const getEarthquakeDataUrl = 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojsonp';
  // Create map
  const map = new $window.mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/dark-v9',
    center: [-103.59179687498357, 40.66995747013945],
    zoom: 3
  });
  const layersStyles = mapService.getMapLayersStyles();
  const updateInterval = 5 * 60 * 1000;
  const strongEarthquakesMagnitudeThreshold = 5;
  let popup = '';
  this.nearCitiesFilter = false;
  this.strongEarthquakesFilter = false;
  this.earthquakes = {};

  const getData = () => {
    $http.jsonp(getEarthquakeDataUrl);
  };
  // Expects features collection
  const filterMapData = earthquakes => {
    earthquakes.features = earthquakes.featuresAll;

    if (this.nearCitiesFilter) {
      const features = $window._.filter(earthquakes.features, item => {
        return item.properties.types.indexOf('nearby-cities') !== -1;
      });

      earthquakes.features = features;
    }

    if (this.strongEarthquakesFilter) {
      const features = $window._.filter(earthquakes.features, item => {
        return item.properties.mag >= strongEarthquakesMagnitudeThreshold;
      });

      earthquakes.features = features;
    }

    return earthquakes;
  };
  // Expects click event and layer index
  const onPinClick = (e, index) => {
    const features = map.queryRenderedFeatures(e.point, {layers: [`earthquakes-${index}`]});

    if (!features.length) {
      return;
    }

    const feature = features[0];

    /* eslint-disable */
    const earthquakeTime = moment.utc(feature.properties.time).format('DD.MM.YYYY HH:mm:ss');
    const earthquakePlace = feature.properties.place;
    const earthquakeMagnitude = feature.properties.mag;
    /* eslint-enable */
    popup = new $window.mapboxgl.Popup()
      .setLngLat(feature.geometry.coordinates)
      .setHTML(`<p><b>Location:</b> ${earthquakePlace}</p><p><b>Time:</b> ${earthquakeTime}</p><p><b>Magnitude:</b> ${earthquakeMagnitude}</p>`)
      .addTo(map);
  };
  this.onFilterChange = () => {
    if (popup) {
      popup.remove();
    }
    this.earthquakes = filterMapData(this.earthquakes);

    if (map.getSource('earthquakes')) {
      map.getSource('earthquakes').setData(this.earthquakes);

      return false;
    }
  };

  $window.eqfeed_callback = data => { // eslint-disable-line
    this.earthquakes = data;
    this.earthquakes.featuresAll = this.earthquakes.features;
    this.earthquakes = filterMapData(this.earthquakes);

    if (map.getSource('earthquakes')) {
      map.getSource('earthquakes').setData(this.earthquakes);

      return false;
    }

    // Display map data and layers
    map.addControl(new $window.mapboxgl.Navigation());
    // Add data source if it doesnt exist yet
    map.addSource('earthquakes', {
      type: 'geojson',
      data: this.earthquakes
    });

    // Create layers based on earthquake magnitude
    angular.forEach(layersStyles, (layer, index) => {
      const isLast = index === (layersStyles.length - 1);

      map.addLayer({
        id: `earthquakes-${index}`,
        type: 'circle',
        source: 'earthquakes',
        paint: {
          'circle-color': layer.color,
          'circle-radius': layer.radius,
          'circle-opacity': layer.opacity,
          'circle-blur': layer.blur
        },
        filter: isLast ?
        ['all',
          ['>=', 'mag', layer.minSize]] :
        ['all',
          ['>=', 'mag', layer.minSize],
          ['<', 'mag', layer.maxSize]]
      });

      map.on('click', e => {
        onPinClick(e, index);
      });

      map.on('mousemove', e => {
        const features = map.queryRenderedFeatures(e.point, {layers: [`earthquakes-${index}`]});

        map.getCanvas().style.cursor = (features.length) ? 'pointer' : '';
      });
    });
  };

  map.on('load', () => {
    getData();

    $window.setInterval(() => {
      getData();
    }, updateInterval);
  });
};

const mapComponent = {
  template: mapTemplate,
  controller: mapController
};

export default mapComponent;
