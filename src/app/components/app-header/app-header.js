const appHeaderTemplate = require('./app-header.html');

const appHeaderComponent = {
  template: appHeaderTemplate,
  bindings: {
    hideLink: '='
  }
};

export default appHeaderComponent;
