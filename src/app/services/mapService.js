const mapService = function () {
  'ngInject';

  const baseSize = 6;
  const baseOpacity = 0.3;
  const layersStyles = [
    {
      color: '#ffcccc',
      radius: baseSize,
      opacity: baseOpacity,
      blur: 0.3,
      minSize: 0,
      maxSize: 2.5
    },
    {
      color: '#ff6666',
      radius: 2 * baseSize,
      opacity: baseOpacity,
      blur: 0.5,
      minSize: 2.5,
      maxSize: 5.4
    },
    {
      color: '#ff0000',
      radius: 3 * baseSize,
      opacity: baseOpacity,
      blur: 0.5,
      minSize: 5.5,
      maxSize: 6
    },
    {
      color: '#b20000',
      radius: 4 * baseSize,
      opacity: baseOpacity,
      blur: 0.6,
      minSize: 6.1,
      maxSize: 6.9
    },
    {
      color: '#660000',
      radius: 5 * baseSize,
      opacity: 0.2,
      blur: 0.9,
      minSize: 7
    }
  ];

  return {
    getMapLayersStyles: () => {
      return layersStyles;
    }
  };
};

export default mapService;
