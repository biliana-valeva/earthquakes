const landingPageTemplate = require('./landing-page.html');
const landingPageController = function ($state) {
  this.linkDisabled = $state.includes('nearMe');
};

const landingPageComponent = {
  template: landingPageTemplate,
  controller: landingPageController
};

export default landingPageComponent;
